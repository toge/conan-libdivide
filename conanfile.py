from conans import ConanFile, tools
import shutil

class LibdivideConan(ConanFile):
    name           = "libdivide"
    license        = "zlib or Boost"
    author         = "toge.mail@gmail.com"
    url            = "https://bitbucket.org/toge/conan-libdivide/"
    homepage       = "https://github.com/ridiculousfish/libdivide"
    description    = "Official git repository for libdivide: optimized integer division http://libdivide.com"
    topics         = ("division", "simd", "header only")
    no_copy_source = True

    def source(self):
        tools.get(**self.conan_data["sources"][self.version])
        shutil.move("libdivide-{}".format(self.version), "libdivide")

    def package(self):
        self.copy("*.h", dst="include", src="libdivide")

    def package_info(self):
        self.info.header_only()
