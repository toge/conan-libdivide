#include <iostream>
#include "libdivide.h"

int main() {
    const libdivide::divider<int64_t> fast_d{523};

    for (int64_t i = 1; i <= 100; ++i) {
        std::cout << (i * 10000) / fast_d << std::endl;
    }

}
